# BRS Website Styleguide

This is the living style guide for the <a href="http://bigroomstudios.com">Big Room Studios website</a>.

Here you will find documentation of common design patterns that are used in this project. It is a resource for developers, designers, and product managers, providing a common language around our design. We use it to maintain modular front-end code and visual consistency across the project.

Please remember to update this reference while making changes to the site.
