---
layout: post
title:  "The Underground’s Finest Project Managers"
teaser: "How project management can sometimes feel like playing drums with four arms."
date:   2015-03-03 00:00:00
author: Sam Frankel
---

Project management, in its purest form, is the practice of managing multiple, often competing, priorities. In his excellent book “[Making Things Happen](http://smile.amazon.com/Making-Things-Happen-Mastering-Management/dp/0596517718/ref=sr_1_1?ie=UTF8&qid=1425832793&sr=8-1&keywords=making+things+happen),” Scott Berkun identifies three sets of concerns that must be accounted for on a project. He labels them the customer, business, and technology perspectives, and goes on to explain why the specific interests of each group are critical for a successful outcome. In his useful framing, success lies in the center of a [Venn diagram](http://en.wikipedia.org/wiki/Venn_diagram) of these interest groups (the center labeled “111” in the figure below).

<figure>
  <img data-src="/assets/images/blog/posts/venn3tab.svg" class="lazyload" alt="Wikipedia &mdash; Venn Diagram as Truth Table">
  <figcaption>Wikipedia &mdash; Venn Diagram as Truth Table</figcaption>
</figure>

As complex as managing the priorities of multiple interest groups can be, the Venn diagram is in some ways a best-case scenario because it implies stability in the emphasis and orientation of these perspectives. But priorities change rapidly, new needs or constraints are discovered, and the relative emphasis on each perspective may change over the life of the project. Project management is like a Venn diagram where the rings are constantly expanding, contracting, and shifting around each other, moving in three-dimensions along an axis of time, and changing color. Also, because we’re really dealing with groups of people and not metaphors, there is a potential for conflict when stakeholders are advocating for their concerns to be held paramount.

As such, project managers tend to be people who enjoy imposing order on disorder, and finding consistent, useful methods to accomplish this. Project managers also tend to be people who enjoy chaos, in limited amounts, whether for its own sake or for the egotism of being relied on to navigate turbulent waters. So, we have a profession that is in some ways inherently stressful, and staffed by people who are in some ways attracted to stressful situations. Thus, the onus for effective stress management is placed squarely on the PM, and I for one can’t understand how anyone does this job without death metal.

Seriously. When my full plate of task management, project coordination, requirements gathering, and dispute resolution gets taken over by an emergency, nothing else is so effective at helping me keep my calm. Project management sometimes feels like playing drums with four arms, and to my mind it demands a soundtrack of the same. Fast, technical death metal, like the [new album from Sarpanitum](https://willowtip.bandcamp.com/album/blessed-be-my-brothers), is perhaps the best audio expression of what it feels like to juggle tasks and priorities so fluidly. And then, when it’s time to put your head down and power through what can seem like an impossibly long, disparate list of tasks, while under fire from the ding of additional email alerts, the [riff-monsters from Bolt Thrower](https://www.youtube.com/watch?v=v-ssrpexU1o) will pull you through every time. Death metal is often very complex, with many different tempos and musical elements, and songs are played with clockwork precision. Musicians are managers of a complex project that play out in real time, and death metal musicians are some of the finest in the world.

<p><iframe class="lazyload" style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1506079330/size=small/bgcol=ffffff/linkcol=2ebd35/transparent=true/" seamless></iframe></p>

<p><iframe class="lazyload" style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=4122618653/size=small/bgcol=ffffff/linkcol=2ebd35/transparent=true/" seamless></iframe></p>

Of course, this has a lot to do with my personal taste. I’m the guy who bikes to work listening to Slayer while weaving through traffic. But I think there’s a legitimate stress management technique here, and the physical force of the music helps bleed off tension during what can be very busy days. In a world of distractions, when you need to alternate between tracking many things and then blocking out that feed in order to focus on immediate, important tasks, I find heavy music to be an ally. Your mileage may vary. But the next time you’re feeling stressed, I challenge you to listen to the following track from Quo Vadis, some of the Montreal death metal scene’s finest project managers, and see if you don’t feel a little more relaxed afterward.

\m/

<iframe class="lazyload" width="420" height="315" src="https://www.youtube.com/embed/W5ciJcksiEU" frameborder="0" allowfullscreen></iframe>
