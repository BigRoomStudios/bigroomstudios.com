---
layout: post
title:  "Westward, Ho!"
teaser: "A Startup Pilgrimage from Portland, Maine to San Francisco."
date:   2015-01-27 00:00:00
author: Sam Mateosian
header_bg_image: "blog/posts/westward-ho.jpg"
---

It’s snowing lightly at our house in Portland, Maine. A squirrel is literally shivering on a branch outside my window. My second daughter is asleep in the stroller downstairs still bundled in her fleece from our brisk walk to the coffee shop.

Meanwhile, the California Bay Area is in the [midst](http://www.usatoday.com/story/news/nation/2014/12/11/north-california-storm-wind-weather/20235905/) of #rainpocalypse due to the “Pineapple Express,” which is possibly the best name for a [meteorological phenomenon](http://en.wikipedia.org/wiki/Pineapple_Express) ever.

That is not the weather I was picturing when we planned our escape from the Maine winter to live in the Bay Area for three months.

<blockquote class="twitter-tweet" lang="en"><p>Actual photo of the <a href="https://twitter.com/hashtag/BayAreaStorm?src=hash">#BayAreaStorm</a> right now. <a href="https://twitter.com/hashtag/rainpocalypse?src=hash">#rainpocalypse</a> <a href="http://t.co/IjhWAw7mrj">pic.twitter.com/IjhWAw7mrj</a></p>&mdash; Zach™ (@zachloker) <a href="https://twitter.com/zachloker/status/543106161869090817">December 11, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Fortunately, weather was not the only factor in our decision to take this “semester abroad” as I’ve taken to calling it. The two other factors were family and business. The family factor is that I have an uncle, aunt, cousin, brother and sister-in-law who live in the area and don’t get to see us or our kids as much as we’d all like.

The last and biggest factor is that, like 99.99% of people my age and younger living in or headed to the Valley, I have startup dreams. Yes, this trek is as cliche as going to Hollywood to become a star. But like it or not, the Bay Area / Silicon Valley is startup Mecca, and I’m about to make the pilgrimage.

That said, I actually happen to think that Portland, Maine is a great place to start the next Facebook. Which is why I use terms like “pilgrimage” and “semester abroad” and not terms like “moving.”

Portland has clearly nailed the Quality of Life thing. Whether it’s [listed](http://www.liveworkportland.org/resources/what-people-say-about-us) as the [#1 Best Summer in America](http://www.travelandleisure.com/americas-favorite-cities/2013/city/portland-me), or the [#9 Best City for Hipsters](http://www.travelandleisure.com/articles/americas-best-cities-for-hipsters-2013/10), or the [#7 Best Beer Town](http://www.10best.com/awards/travel/best-beer-town/), or the [#8 Best Coffee City](http://www.travelandleisure.com/articles/americas-best-coffee-cities/9#) or the [#3 Best City for Families](http://www.parenting.com/gallery/best-cities-to-raise-a-family-2012?pnid=558898) or the #2 Best City for Bands that Sound Like Death Cab, Portland is unequivocally a hip, fun, nice, safe, beautiful place to live.

That “Quality of Life Thing” is drawing smart, creative, passionate people in droves, like hipsters to an Edison lightbulb. Many of them are returning to Maine from a first career in the “big leagues” at companies like Google, Facebook or Microsoft. They’re often looking for a second or third career where they can do smart, interesting, important things while maintaining better work / life balance, spending more time with their kids and having more connection with the natural world.

So, are we headed to #1 Best Small City for Building Your Startup?

I asked [Jason Cianchette](https://twitter.com/cianchette), founder of [Liquid Wireless](http://liquidwireless.com/), a fellow Mainer and serial entrepreneur, if he saw any advantage to building a digital company in this town. He couched his answer: “Portland is a unique city and there are unique advantages and challenges to starting a company here.” In other words: yes and no.

We have challenges, for sure. There have been many times in the last decade or so of trying to build a great digital company in Portland where I’ve thought I’m fighting a stupidly uphill battle.

<blockquote>Why am I trying to do something big in a place that celebrates how small all of its business are?</blockquote>

And then there’s the god-awful proposed moniker for our startup region: “[Silicon Lobster](http://contributors.pressherald.com/business/the-startupline/new-york-tech-company-betting-portland/),” --- it’s as if people are incapable of doing anything in Maine without mentioning lobsters or lighthouses --- and now I’m at fault too (Unlike “valley,” “prairie” or “alley,” lobster is not even a kind of place! So, thanks, but no.).

In the wins column, we agreed that being a cool fish (don’t say lobster!) in a small pond is an advantage. Jason contends, “we have access to great talent in Portland that would be very hard to recruit and retain in Silicon Valley.”

There are arguably only a handful of digital companies in Portland that have the cool, growing startup vibe. One of the few perks of being in a fledgling but “hot” startup city is that those of us with companies in that position have unique access to high-level incoming talent. In a larger ecosystem there would be a lot more competition for that talent.

More distributed workplaces means that a fair amount of the talented folks arriving here are keeping their Boston-, NY- or SF-based jobs. While a loss for the job market, it’s a win for the community as they come bearing great experience and connections.

Our high level of job retention is a huge perk. In the 12 years of running our first company, [Big Room Studios](http://bigroomstudios.com), we’ve had essentially zero churn. The atmosphere is full of camaraderie and playful banter. Our seasonal holiday party feels like a big family gathering. I sometimes take it for granted, but what we have built here is incredibly special.

<blockquote>“It’s the schools, the restaurants, the community support, the access to nature,” says Jason.</blockquote>

That community support is another major advantage he says, “we all want these companies to be successful and so we all help each other out.”

We’ve certainly received huge support from the community and even from [state-funded grants](http://www.mainetechnology.org/). In a more competitive landscape we might not have the space to grow big enough to be successful before being eaten by larger fish.

Again, it’s that quality of life thing. Fundamentally, that’s what keeps and brings people like me and Jason here.

## Startup Diaspora

It’s not just Portland, there are quality-of-life hot-spots all across America. Places like Madison, Boulder and Austin have all grown from small to mid-size cities after winning on the hipster-quality-of-life-index. And, from my work on the [Maine Startup & Create Week](http://mainestartupandcreateweek.com/) conference, we’ve seen we’re not alone as an outlier. There are startup communities popping up all over the world, from Little Rock, Arkansas to Yerevan, Armenia. Like me, the people in these communities share a strong sense of place, and a sense of purpose in improving the economic landscape of those places.

Interesting people want to live in great places. And, interesting people start interesting companies. [Paul Budnitz](http://paulbudnitz.com/), founder of [Kidrobot](http://www.kidrobot.com/), [Budnitz Bicycle](http://budnitzbicycles.com/) and most recently the alt-social-network, [Ello](http://ello.co/), is a prime example. He’s a serial entrepreneur of successful, interesting, hipster-fabulous companies working out of places like Boulder and Burlington.

One of the reasons that Ello can be based in Burlington and play on the same field as Facebook is that we’re actually realizing the dream that the internet can democratize access to information. All of the tools, information and skills training for building something like Facebook are now accessible to more or less anyone with a laptop, the internet and a decent brain.

## The Dream of the 90s is Alive in Portland

It’s happened so quickly we haven’t actually noticed it as change. Facebook was made possible in part because of the ease and availability of PHP. It was the combination of internet access, open source software and documentation that enabled a smart kid in a dorm room to build a powerful communications platform in a few caffeine-fueled evenings.

There is so much more of that kind of availability now --- think: github, stack overflow, code academy, medium, rails and node.js. None of those things existed when the Zuck started hacking on thefacebook.com and they all make it easier for someone, somewhere to build a great digital, world-changing product.

The open / collaborative / sharing world that we live in today is largely the result of the Torvalds ideology winning, and a distinct departure from the closed / competitive / Jobs / Gates ideology of yesterday.

It is incredibly fortunate that the greater digital startup community has embraced the open-source model and shares almost all of its most important tools and learnings --- everything from great software to podcasts on starting and growing companies. It’s good for the world. It means that the next Facebook could come out of Bangladesh or Yerevan (or Portland!).

Well, it could, except for a couple of other very important factors, namely: access to capital, and risk tolerance.

Facebook’s success hinged perhaps less on Zuckerberg’s tech than his early access to funding via wealthy friends, willingness to go after more funding in the Valley, grit, raw smarts, determination, lucky breaks, and everything else that goes into growing something from a prototype to a multi-billion dollar business. I believe that a key component in that pursuit is bravery.

## Westward the Course of Empire Takes Its Way

At least three waves of human migration occurred to make California the heartland of the adventurous. One, from the Old World to the New: people seeking ideological freedom combined with brave entrepreneurs to found America (and accidentally slash on-purpose kill most of the native inhabitants). Two, from the East Coast to the West: more adventurous people settled in the middle of effing nowhere and make a life out of sticks and rocks. Many pursued risky speculation on mining for gold. Three, the cultural immigration of the ‘60s: psychonauts and “free thinkers” converged on Berkeley and SF to explore the frontier of consciousness.

My thesis is that all of that adventurous, freedom-seeking, self-selection primed California to be the modern world’s preeminent place for innovation, from which we have garnered both Hollywood and the Digital Age. And, in recent history, perhaps there has been a fourth wave of smart, adventurous people seeking their fortune by dropping out and starting up.

All of the great successes coming out of the Valley in the last three decades have driven an incredible amount of wealth. Wealth plus the willingness to take risky bets on new ventures creates more wealth and more great inventions. It creates a positive feedback loop of innovation.

And there’s nothing wrong with that. Well, almost nothing. There is a ton right with that formula --- and it’s precisely what we need more of in places like Portland. The only criticism that I will lay against the place I’m about to make my home for three months (don’t hate me, I just called it the heartland of the adventurous!) is this:

## Positive Feedback is Not Always a Positive

Positive feedback loops are potent and prone to crash. They make things like black holes (which is, in fact, an example of an extremely massive positive feedback loop) --- where nothing comes out. The wealth stays in the Valley because the investors only want to invest in companies that are in the Valley. And, consequently, loads of talented people go to work for those companies because they have all the money and cool jobs.

<figure>
  <img data-src="/assets/images/blog/posts/black-hole.jpg" class="lazyload" alt="Black Hole">
  <figcaption>What would you see if you went right up to a black hole? <a href="http://apod.nasa.gov/apod/ap141026.html">Featured</a> is a <a href="http://ascl.net/9910.006">computer generated</a> image highlighting how strange things would look.</figcaption>
</figure>

Eventually you get to a state where everyone who’s there works for a digital startup and they all have more money than they need. You get echo chamber effects where large majorities start to share the same thought patterns, behaviors and mannerisms. All the entrepreneurs and startup pitches start to sound bizarrely similar, which is why a show like Mike Judge’s “[Silicon Valley](http://www.hbo.com/silicon-valley#/)” is so funny.

SF is certainly plenty diverse, and people are arriving there every day from all over the world. It’s maybe what happens after they arrive, when living inside of the digital revolution becomes the new normal. Then, maybe, like [oscillators syncing](https://www.youtube.com/watch?v=kqFc4wriBvE), uniformity emerges.

Here’s Jason again: “In Maine, 99% of the people I interact with are not engineers, when I go to San Fran it’s the opposite. I know if my stuff works for someone in Westbrook it’ll work in the rest of America.” We agree, “most of America looks a lot more like Westbrook than San Francisco.”

This is my outsider’s perspective, and I’m fully prepared to be wrong. Another friend from the Portland startup community, [Sarah Hines](https://twitter.com/sarah_hines), (who is used to telling my how and why I’m wrong) suggests that perhaps I should “use the more positive image of a sun rather than a black hole.” Maybe that’s better, especially considering the warmth we’re seeking with this trip.

I certainly get the appeal of living there. I too want to live in a future where everyone has access to massive amounts of bandwidth, works on things that interest them, knits their own iPhone cases and has no requirement to stockpile firewood or heating oil.

Stephen Hawking proved that [black holes miraculously emit radiation](http://en.wikipedia.org/wiki/Hawking_radiation). So too, fortunately, some of the talent and money does escape the pull of the Valley. Certainly, an enormous amount of money leaves via philanthropy. And, another huge share leaves via spending in other economies.

But, I think there is a strong case to be made for the Valley investor community to look for more hits coming out of odd places. The Digital Age has enabled people everywhere to create new great products, and those people have ways of thinking that are outside of the box of the Valley, specifically because they do not live in that box. If you force all of them into one box in order to fund their ideas, you’ll lose countless opportunities, not to mention contrary points of view.

So that’s why I’m going *and* why I’m not moving there full time. While I fully appreciate the amazing amount of awesome talent and wickedly cool shit that’s happening in California, I happen to really, really like where I’m from.

I think that my sense of place is an important enough cause to continue fighting the uphill battle. And, that there’s a chance that it might actually turn out to be an advantage in the end.

And, I really don’t want to live anywhere else. That is, other than the three freezing, dark months of winter for which I’ll happily move anywhere warm and sunny.

Ok, I’ll take rainy and not freezing.

---

*This post was [originally published on Medium](https://medium.com/@semateos/westward-ho-9be8c9a42c8b) on December 25, 2014.*
