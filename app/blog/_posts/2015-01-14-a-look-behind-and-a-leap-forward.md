---
layout: post
title:  "A Look Behind, and a Leap Forward"
teaser: "The carpet finally matches the drapes, and we couldn’t be more excited."
date:   2015-01-14 00:00:00
author: Chris Bracco
---

Hello, friends and cohorts! You may have noticed that things look a bit different in this corner of the web. After much trepidation, we’ve settled on a new brand and website for [Big Room Studios](http://bigroomstudios.com). The carpet matches the drapes, and we couldn’t be more excited.

Redesigning yourself is daunting. There’s no way around that simple fact. Many offer advice *against* such trying endeavors, but we view it as a challenge, and an opportunity to peer inside of ourselves to see what we can dig out and discover.

BRS has been working steadfast since 2002, having grown from a couple of dingbats in a home office to a professional team of talented individuals who have worked with hundreds of clients, big and small. We’ve been designing brands and building useful web applications since the [first dot-com bubble](https://en.wikipedia.org/wiki/Dot-com_bubble) burst, and we haven’t slowed down since. In fact, we’ve expanded our skillset to [a wide range of services](/services/) that keep our clients coming back for more.

### Ultimately, we enjoy making awesome things and putting them out into the world.

For a long time, our work has done the talking for us. But now, in 2015, we are finally ready to do some of the talking ourselves, and share our ideas and our plans for the future in a more tangible way.

## What’s to come of this?

The Big Room blog will be a place for both work and play, a mostly intelligible and sometimes ridiculous stream of consciousness ranging from company news to programming tutorials, code snippets to geek rants, workflow tips to holiday greeting cards.

We already have a few entries in the backlog and many ideas in the icebox, so don’t close that browser tab. Or do, but consider [subscribing to the RSS feed](/feed.xml) so you remember to come back.

---

*If you have any suggestions, ideas, or topics that you would like us to write about, leave a comment beneath this entry and it will be considered. Thank you!*
