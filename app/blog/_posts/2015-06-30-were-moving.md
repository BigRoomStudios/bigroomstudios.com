---
layout: post
title:  "We’re Moving!"
teaser: "Come September 2015, the newly established Thompson’s Point will be the next place for us to call home."
date:   2015-06-30 00:00:00
author: Big Room Studios
header_bg_image: "blog/posts/were-moving.jpg"
---

It’s been an exciting, fast-moving year for Big Room Studios so far. Where have the past six months gone? With many new projects and faces, we’ve suddenly found ourselves at capacity in [our current location]({{ site.location_directions_url }}).

After an extensive (and sometimes frustrating) search across Portland and the surrounding area, we’ve decided that the Brick North building at Thompson’s Point will be the next place for us to call home. We will be sharing *The Point* with an eclectic group of businesses and organizations, such as the [State Theatre](http://statetheatreportland.com) (who’ve already lined up some exciting concerts for this summer), the [Open Bench Project](http://obportland.org/), the [Circus Conservatory](http://circusconservatory.org), and (by Spring 2016) the [International Cryptozoology Museum](http://cryptozoologymuseum.com). It’s a thrilling time for us, and we are looking forward to what the future holds.

<p><iframe class="lazyload" width="420" height="315" src="https://www.youtube.com/embed/XrqprupfQWo" frameborder="0" allowfullscreen></iframe></p>

Our co-founder and resident brainiac Sam Mateosian was quoted in a [recent Mainebiz article](http://www.mainebiz.biz/article/20150630/NEWS0101/150639999), stating: 

<blockquote>
  <p>We believe in bold steps and are always willing to take bets on young technology, projects, and people who show exceptional promise. To that end, we are excited to be in on the ground floor of one of the most creative and novel developments in Portland's history.</p>
</blockquote>

As the first “office-based tenant” to join *The Point*, we can’t wait to throw ourselves into the mix, and hope to be a positive addition to the diverse community that is currently manifesting there. 

Onward and upward!
