---
layout: services_single
title: "Identity"
subtitle: "An Awesome Brand"
tagline: "Leadership. Uniqueness. Authenticity."
description: "Well-executed brands stand out from their competition. We design memorable brand experiences for startups, corporations, organizations, and more."
feature_list:
  - feature: "Brand Strategy Consultation"
  - feature: "Custom Design"
  - feature: "(10) Brand Concepts"
  - feature: "(3) Revision Rounds"
  - feature: "Print Materials"
  - feature: "Product Labels"
  - feature: "Business Cards"
order: 2
date: 2015-02-23 00:00:01
header_bg_image: "services/header-bg-identity.jpg"
---

Awesome brands stand out from their competitors and get noticed. Your company brand is much more than a simple logo, it is how you choose to be identified to the world. We can create new brands from scratch or build on an established logo and design style. BRS *Identity* provides memorable brands for projects, companies, and enterprise-level corporations. We take the time to research and explore all possibilities in order to build the perfect *Identity* for our clients.

### Discovery

Behind your brand is an organization responsible for products or services that you’re excited to share. With *Identity*, you’ll spend time with our team to uncover what makes your company unique, then we’ll draft a brand strategy based on what we have learned. This includes: 

- 2-hour onsite meeting or video call to understand your organization and the goals of the brand or campaign
- Discussion and preparation of image-based “mood board” to identify and select design themes
- A brand map of your identity needs
- Discussion of the intended uses of the brand, in order to further guide brand strategy and identify additional needs or plan the hand-off to internal or external marketing teams


### Brand Collateral

Now it’s time to build off the discovery outline. We offer up to ten *Identity* brand concepts and three rounds of revisions. The revision process narrows from an initial set of concepts to a single, refined logo or final brand. Customized products for consistent branding can include:

- Style guide / Brand documentation 
- Business cards
- Stationary
- Online banner advertisements 
- Powerpoint templates
- Other promotional products (event banners, tee shirts, gift boxes, sunglasses - challenge us!)
