---
layout: services_single
title: "Evolution"
subtitle: "An Awesome Service Plan"
tagline: "Utilitarian. Fundamental. Adaptive."
description: "Already have a product? We offer rigorous testing to give you an idea how easy and enjoyable it is to use, plus a customized roadmap for improvement."
feature_list:
  - feature: "Content Management"
  - feature: "Photo Editing"
  - feature: "Google Analytics Reporting"
  - feature: "Google Apps / Email for Business"
  - feature: "Design Tasks"
  - feature: "Minor Site Updates & Bug Fixes"
  - feature: "Technical Consultations"
  - feature: "Updating / Exporting Data"
order: 5
date: 2015-02-23 00:00:00
header_bg_image: "services/header-bg-evolution.jpg"
---

### Product Testing

Already have a product? We offer rigorous testing to give you an idea how easy and enjoyable it is to use, plus a customized roadmap for improvement.

### Internal Design Review

How well does your product comply with established principles of user interface design? We can tell you. Our assessment will outline problematic areas along with brief analyses and suggested solutions.

### Usability Testing

We’ll place your product in front of your target audience to gain insight into how how easy — or difficult — it is to use. What you’ll get is a prioritized list of usability issues and suggested routes to take your product to the next level.

### Maintenance: Keep the Wheels Turning

Websites and applications continue to evolve after initial development. We’re prepared to support your technology project based on your growing needs, and the needs of your customers.

- Content Management
- Photo editing
- Google Analytic reports
- Google Email for Business configuration and setup 
- Design tasks
- Minor site updates
- Technical consultation 
- Updating or exporting data

### Hosting: Uptime, Powered by People

### Website Hosting

The files that make up a web application are stored and delivered to visitors by servers. The speed of your web application and the security of your customer data depend on the quality of the server that stores and delivers these files. This is service is called hosting. And, like any kind of host, the job is to be friendly and quickly attend to the needs of your visitors.

There are many inexpensive hosting services. Their economy of scale is achieved by running servers at close to maximum capacity, offering little individual attention, and providing minimal technical support. We offer a hands-on, human approach from configuration to updates, and run our servers at low capacity for faster service. So come on in, we’ll make sure your all files feel welcome and safe. And then we’ll get them to where they’re headed as quickly as possible.

### Application Scaling

We understand that your application may have different needs and requirements than your 
average hosting client. By using Amazon Web Services, we can efficiently and appropriately scale over time while only utilizing the services you need at that moment. Whether you have a few users, or a few million, we have the solution for you.
