---
layout: work_single
title: "Thos. Moser"
description: "We partnered with them to create a custom and mobile-friendly splash page that captured the essence of the Thos. Moser brand, highlighting their products and philosophy."
project_url: "http://www.thosmoser.com"
order: 11
date: 2015-02-23 00:00:11
header_bg_image: "work/header-bg-image.jpg"
images:
  archive_grid: "thos-moser/archive-work-grid.jpg"
  single_main: "thos-moser/single-work-main.jpg"
  single_grid1: "thos-moser/single-work-grid1.jpg"
  single_grid2: "thos-moser/single-work-grid2.jpg"
  single_grid3: "thos-moser/single-work-grid3.jpg"

tags:
- Origin
---

### The Goal

Local furniture designer Thos. Moser is renowned for handcrafted and timeless furniture that celebrates the natural beauty of wood. After years of success, they had a creative shift in their branding direction and needed a revamped site to match. 

### Our Approach

Thos. Moser approached Big Room Studios with a vision for a new site but knew that they couldn’t do a full website overhaul. We partnered with them to create a custom and mobile-friendly splash page that captured the essence of the Thos. Moser brand, highlighting their products and philosophy. The interior pages were re-imagined with an updated color scheme, fonts, and consistent imagery to reflect the new Thos. Moser identity. 
