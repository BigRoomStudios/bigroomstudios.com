---
layout: work_single
title: "Atlantic Fund Services"
description: "Atlantic Fund Services realized the importance of having a responsive website that can be viewed optimally on a wide array of devices. A long time customer, they came to Big Room Studios for this important website redesign."
project_url: "http://atlanticfundservices.com"
order: 9
date: 2015-02-23 00:00:09
header_bg_image: "work/header-bg-image.jpg"
images:
  archive_grid: "atlantic/archive-work-grid.jpg"
  single_main: "atlantic/single-work-main.jpg"
  single_grid1: "atlantic/single-work-grid1.jpg"
  single_grid2: "atlantic/single-work-grid2.jpg"
tags:
- Origin
---

### The Goal

Atlantic Fund Services realized the importance of having a responsive website that can be viewed optimally on a wide array of devices. A long time customer, they came to Big Room Studios for this important website redesign.

### Our Approach

Not only did we redesign their site, but we created a brand new, fully responsive and custom layout for them, along with a great content management system. Big Room Studios has supported Atlantic Fund Services since they first opened their doors. Our work has included several site iterations over the years, in addition to a wide range of design services including brand, logo design, banner and print advertising, tradeshow booths, and more.
