---
layout: work_single
title: "Scott Miller Style"
description: ""
project_url: "http://www.scottmillerstyle.com"
order: 5
date: 2015-02-23 00:00:05
header_bg_image: "work/header-bg-image.jpg"
images:
  archive_grid: "scott-miller/archive-work-grid.jpg"
  single_main: "scott-miller/single-work-main.jpg"
  single_grid1: "scott-miller/single-work-grid1.jpg"
  single_grid2: "scott-miller/single-work-grid2.jpg"
  single_grid3: "scott-miller/single-work-grid3.jpg"
tags:
- Origin
---

### The Goal

Scott Miller Style, a hair salon and high-end beauty product store in NY state, needed a more responsive website to accommodate the rising use of mobile devices. They wanted to keep their existing aesthetic that highlights their luxurious experience, specialized talents, and creativity that the Scott Miller brand has built for over 30 years.

### Our Approach

Another long-time client of Big Room, Scott Miller Style wanted to keep their existing backend CMS intact, and redesign the front-end to work well across many device types, from mobile to desktop. Working within the constraints of their existing look-and-feel, we redesigned the Scott Miller website to be more mobile-friendly and still retain the minimalist, black-and-white aesthetic that serves the Scott Miller brand so well.
