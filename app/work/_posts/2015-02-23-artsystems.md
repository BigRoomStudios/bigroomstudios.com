---
layout: work_single
title: "Artsystems"
description: "Artsystems wanted to bring their gallery software suite to the cloud, expanding on a successful desktop application for managing artwork collections."
project_url: "http://www.artsystems.com"
order: 4
date: 2015-02-23 00:00:04
header_bg_image: "work/header-bg-image.jpg"
images:
  archive_grid: "artsystems/archive-work-grid.jpg"
  single_main: "artsystems/single-work-main.jpg"
  single_grid1: "artsystems/single-work-grid1.jpg"
  single_grid2: "artsystems/single-work-grid2.jpg"
  single_grid3: "artsystems/single-work-grid3.jpg"
tags:
- Catalyst
- Enterprise
---

### The Goal

Artsystems wanted to bring their gallery software suite to the cloud, expanding on a successful desktop application for managing artwork collections. Their goal was to create something user-friendly and powerful, giving galleries the ability to organize and publish artwork collections, exhibitions, news, press and more.

### Our Approach

Big Room Studios developed a Software as a Service (SAAS) web platform allowing Artsystems to create gallery sites and customize each site to their customer’s need. This multi-tenant system integrates directly with the desktop and iPad applications built by Artsystems. With over 100 active gallery sites, this platform allows substantial volumes of data to be imported and exported by users with ease.
