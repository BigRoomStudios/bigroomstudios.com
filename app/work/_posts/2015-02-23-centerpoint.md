---
layout: work_single
title: "Centerpoint"
description: "Centerpoint is an epic science fiction experience set on a distant alien world where players work together to gather resources, create societies, and explore the vast expanse of the planet."
project_url: "http://centerpointgame.com"
order: 10
date: 2015-02-23 00:00:10
header_bg_image: "work/header-bg-image.jpg"
images:
  archive_grid: "centerpoint/archive-work-grid.jpg"
  single_main: "centerpoint/single-work-main.jpg"
  single_grid1: "centerpoint/single-work-grid1.jpg"
  single_grid2: "centerpoint/single-work-grid2.jpg"
  single_grid3: "centerpoint/single-work-grid3.jpg"
tags:
- Catalyst
---

### The Goal

Centerpoint is an epic science fiction experience set on a distant alien world where players work together to gather resources, create societies, and explore the vast expanse of the planet. Combining gameplay from different genres, Centerpoint is a real-time first person strategy game in persistent, massive multiplayer environment. It is currently in active development and is available for download on the Centerpoint web site. (link:http://centerpointgame.com/)

### Our Approach

Centerpoint is being built on Unity 3D as a cross platform game with current availability on PC, Mac and Linux. Big Room Studios meets this project with a large skill set including concept art, 3D design, game development, API development and fault tolerant server architecture. Technologies used in this project include Mono (C#), NodeJS / Hapi , MongoDB, among common web technologies such as HTML,CSS, and Javascript.
