---
layout: work_single
title: "NextWorth"
description: "Nextworth consulted with Big Room Studios to develop a Minimum Viable Product (MVP) for a used iPhone trade-in pricing calculator so that they could begin trading in used iPhones online."
project_url: "https://nextworth.com"
featured: true
order: 3
date: 2015-02-23 00:00:03
header_bg_image: "work/header-bg-image.jpg"
images:
  triptych:
    aside1: "nextworth/triptych-aside1.jpg"
    aside2: "nextworth/triptych-aside2.jpg"
    main: "nextworth/triptych-main.jpg"
  archive_grid: "nextworth/archive-work-grid.jpg"
  single_main: "nextworth/single-work-main.jpg"
  single_grid1: "nextworth/single-work-grid1.jpg"
  single_grid2: "nextworth/single-work-grid2.jpg"
  single_grid3: "nextworth/single-work-grid3.jpg"
tags:
- Catalyst
- Enterprise
---

### The Goal

When entrepreneur Dave Chen was getting his startup off the ground, he needed a way to validate and begin iterating on his concept of trading in used iPhones. Nextworth consulted with Big Room Studios to develop a Minimum Viable Product (MVP) for a used iPhone trade-in pricing calculator so that they could begin trading in used iPhones online.

### Our Approach

In true lean startup fashion, the MVP underwent many market driven pivots and eventually evolved  into a business critical Enterprise Resource Planning (ERP) system and Software as a Service (SaaS) platform. As the MVP grew and the business scaled, BRS collaborated with NextWorth to turn business processes into a piece of flexible technology. As success of the technology created a demand for additional features, BRS worked to mature the MVP into an enterprise-level application. This included IT infrastructure, core platform development, as well as third-party integrations for geolocation (Google), payment processing (i.e. StoreFinancial, Ceridian, Incomm), inventory and resale management (eBay, Amazon, ChannelAdvisor), and automated shipment assistance (USPS, FedEx, UPS). The platform now runs on a private cloud, generates millions of dollars in annual revenue, processes hundreds of transactions a second, and has been integrated into over 5,000 brick-and-mortar retail locations.
