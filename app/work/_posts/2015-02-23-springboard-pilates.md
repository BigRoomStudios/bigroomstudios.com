---
layout: work_single
title: "Springboard Pilates"
description: "A longstanding client of Big Room Studios, Springboard Pilates approached us in 2014 with an itch to redesign their studio’s website."
project_url: "http://www.springboardpilates.com"
order: 6
date: 2015-02-23 00:00:06
header_bg_image: "work/header-bg-image.jpg"
images:
  archive_grid: "springboard/archive-work-grid.jpg"
  single_main: "springboard/single-work-main.jpg"
  single_grid1: "springboard/single-work-grid1.jpg"
  single_grid2: "springboard/single-work-grid2.jpg"
  single_grid3: "springboard/single-work-grid3.jpg"
tags:
- Origin
---

### The Goal

A longstanding client of Big Room Studios, Springboard Pilates approached us in 2014 with an itch to redesign their studio’s website. Their existing website was outdated and difficult to use, and needed a complete overhaul to better match their brand and provide a better user experience for their members.

### The Approach

We assembled a small team (content strategist & front-end developer) and worked swiftly to completely reinvent their online presence. Using a “content-first” approach, we began by rewriting and revitalizing their existing copy to better match their company’s culture and brand. Doing the content homework early on, rather than later in the process, allowed our front-end developer to both “design in the browser” and develop the website functionality in tandem. We chose WordPress as the CMS platform for this project, so that Meredith and the rest of the Springboard team could maintain their blog and easily share updates with their members via social media.

They do a great job of keeping their blog fresh and relevant, and [it's chock full of great exercising tips, health advice, and more!](http://springboardpilates.com/journal/)
