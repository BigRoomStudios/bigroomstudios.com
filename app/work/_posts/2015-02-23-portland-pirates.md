---
layout: work_single
title: "Portland Pirates"
description: "Big Room Studios worked with the Pirates on a content driven design, and then developed a new responsive site using the WordPress CMS."
project_url: "http://portlandpirates.com"
order: 8
date: 2015-02-23 00:00:08
header_bg_image: "work/header-bg-image.jpg"
images:
  archive_grid: "portland-pirates/archive-work-grid.jpg"
  single_main: "portland-pirates/single-work-main.jpg"
  single_grid1: "portland-pirates/single-work-grid1.jpg"
  single_grid2: "portland-pirates/single-work-grid2.jpg"
  single_grid3: "portland-pirates/single-work-grid3.jpg"
tags:
- Origin
---

### The Goal

The Portland Pirates hockey team had a few bumpy years, including playing the 2013-2014 season out of town. In preparation for the 2014-2015 season --- back in Portland, with changes in ownership, management, arena, and players --- the Pirates decided to redesign their dated website.

### Our Approach

Big Room Studios worked with the Pirates on a content driven design, and then developed a new responsive site using the <a href="http://wordpress.org">WordPress CMS</a>. The new site highlighted the information most important to fans, and allowed the Pirates to add and edit additional blog posts, videos, and game results throughout the season. The project was designed and developed in just a few months, and the Pirates were selling tickets on the new site well in advance of opening day!
