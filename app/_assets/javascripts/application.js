var $ = require('jquery');
require('lazysizes');
require('howler');
require('headroom');
require('jquery-headroom');
require('jquery-fitvids');
require('jquery-pagepiling-pageover');
require('jquery-scroll-depth');

/**
 * Debounced onresize function
 */

function doResize(c, t) {
    onresize = function() {
        clearTimeout(t);
        t = setTimeout(c, 100)
    };

    return c;
};

/**
 * When the DOM is ready,
 */

$(function() {

  /**
   * Variables
   */

  var // Generic
      $window = $(window),
      htmlbody = $('html, body'),
      html = $('html'),
      body = $('body'),
      // Site
      site = $('.site'),
      siteIntro = $('.site-intro'),
      siteHeader = $('.site-header'),
      siteHeaderHeight = $('.site-header').height(),
      siteNav = $('.site-nav'),
      siteNavToggle = $('.nav-toggle'),
      siteMain = $('.site-main'),
      siteHeaderHeadroomOptions = {
        'classes': {
          'top': 'toolbar-top',
          'notTop': 'toolbar-not-top',
          'initial': 'animation',
          'pinned': 'slide-down',
          'unpinned': 'slide-up'
        },
        'offset': 180,
        'tolerance': {
          down: 10,
          up: 20
        }
      },
      // Post
      postContent = $('.post-content'),
      isTouch = Modernizr.touch,
      isSmall = Modernizr.mq('only all and (max-width: 599px)'),
      transitionEndEventNames = {
        'WebkitTransition': 'webkitTransitionEnd',
        'MozTransition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'msTransition': 'MSTransitionEnd',
        'transition': 'transitionend'
      },
      transitionEndEventName = transitionEndEventNames[Modernizr.prefixed('transition')],
      // Home
      sectionTeam = $('.section-team'),
      instagramAccount = {
        access_token: '331102874.cf6f8ac.1d030b8a6ae644ba84dc1c9237806905'
      },
      instagramCount = isSmall ? 4 : 8,
      instagramPhotos = [],
      instagramPhoto = $('.instagram-photo'),
      instagramWrapper = $('.instagram'),
      instagramSpinner = $('.js-spinner'),
      // Persons
      personToggle = $('.js-person-toggle'),
      person = $('.person'),
      // Sounds
      bgMusic = new Howl({
        urls: [
          'http://onholdsamples.com/samples/music/IN-113.mp3'
        ],
        loop: true,
        buffer: true
      }),
      meow = new Howl ({
        urls: [
          '/assets/audios/meow.mp3',
          '/assets/audios/meow.ogg'
        ],
        onend: function() {
          if(body.hasClass('is-pet-mode')) {
            bgMusic.play();
          } else {
            bgMusic.pause();
          }
        }
      }),
      // Forms
      formContact = $("#formContact"),
      formAnimatedInput = $('.form-animate-inputs .form-input'),
      // Services
      bgMouseoverEffect = $('.js-bg-mouseover-effect');

  /**
   * Init
   */

  // Site: Header w/ Headroom.js
  siteHeader.headroom(siteHeaderHeadroomOptions);

  // Site: Nav Toggle
  siteNavToggle.on('click', function() {
    $(this).toggleClass('is-active');
    siteNav.toggleClass('is-open');
    body.toggleClass('site-nav-open');
    siteNav.one(transitionEndEventName, function() {
      site.toggleClass('no-scroll');
    });
  });

  // Site: Google Analytics Custom Events Tracking
  $.scrollDepth({
    minHeight: 2000,
    elements: ['#calls-to-action']
    // eventHandler: function(data) {
    //   console.log(data);
    // }
  });

  $('#header-phone').on('click', function() {
      ga('send', 'event', 'Header actions', 'click', 'Phone');
  });

  $('#header-email').on('click', function() {
      ga('send', 'event', 'Header actions', 'click', 'E-mail');
  });

  $('#header-hamburger').on('click', function() {
      ga('send', 'event', 'Header actions', 'click', 'Hamburger');
  });

  $('#cta-contact').on('click', function() {
      ga('send', 'event', 'Calls-to-action', 'click', 'Contact Us');
  });

  $('#contact-phone').on('click', function() {
      ga('send', 'event', 'Contact Page', 'click', 'Phone');
  });

  $('#contact-directions').on('click', function() {
      ga('send', 'event', 'Contact Page', 'click', 'Get Directions');
  });

  $('#contact-email').on('click', function() {
      ga('send', 'event', 'Contact Page', 'click', 'E-mail');
  });

  formContact.each(function() {
    var jqForm = $(this);
    var jsForm = this;
    var action = jqForm.attr("action");
    jqForm.submit(function(event) { // when someone submits the form(s)
      event.preventDefault(); // don't submit the form yet
      ga('send', 'event', 'Forms', 'submit form', 'Contact Form');
      setTimeout(function() { // now wait 300 milliseconds...
        jsForm.submit(); // ... and continue with the form submission
      }, 300);
    });
  });

  // Post: Initialize fitVids
  postContent.fitVids();

  // Home
  if(body.hasClass('page-home')) {

    // Home: Intro
    siteIntro.pagepiling({
      sectionSelector: '.slide',
      keyboardScrolling: false,
      pageOver: siteMain
    });

    // Home: Team Video Fallback
    if(isTouch) {
      sectionTeam.addClass('video-fallback');
    }

    // Home: Lazyload Instagram AJAX
    var instagramReady = true,
        instagramWrapperPosition = instagramWrapper.offset().top;

    var instaScrolled = false;

    $(window).scroll(function() {

      var instagramScrollBottom = $(window).scrollTop() + $(window).height();

      // Update wrapper position when scrolling begins
      if (!instaScrolled) {
        instagramWrapperPosition = instagramWrapper.offset().top;
        instaScrolled = true;
      }

      if(instagramReady && (instagramScrollBottom >= instagramWrapperPosition)) {

        instagramReady = false;

        $.ajax({

          type: 'GET',
          dataType: 'jsonp',
          cache: false,
          url: 'https://api.instagram.com/v1/users/self/feed/?access_token=' + instagramAccount.access_token + '&callback=?',
          beforeSend: function() {
            instagramSpinner.addClass('is-visible');
          }

        }).done(function(response) { // Great success!

          setTimeout(function() {

            instagramSpinner.removeClass('is-visible');

            for (var i = 0; i < instagramCount; i++) {

              // Print response to the console
              // console.log(response);

              // Handle 400 error
              if(response.code === '400') {
                alert(response.error_message);
                return false;
              }

              // Store response data
              var photoPageUrl = response.data[i].link,
                  photoImageUrl = response.data[i].images.standard_resolution.url,
                  photoRawDate = response.data[i].created_time,
                  photoUsername = response.data[i].user.username,
                  photoUserUrl = 'https://instagram.com/' + photoUsername,
                  photoTags = response.data[i].tags,
                  photoDate = new Date(photoRawDate * 1000),
                  photoMonths = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                  ],
                  photoMonth = photoMonths[photoDate.getMonth()],
                  photoDay = photoDate.getDate(),
                  photoYear = photoDate.getFullYear(),
                  photoDateFormatted = photoMonth + ' ' + photoDay + ', ' + photoYear;

              // Exclude images with the #nsfw hashtag
              if($.inArray('nsfw', photoTags) > -1) {

                // Increment image count so it will always display the correct amount
                instagramCount++;

              } else {

                // Create & insert image markup
                $('<a id="instagram-photo-' + (i + 1) + '" href="' + photoPageUrl + '" class="instagram-photo fl 1/2 xs-1/4" target="_blank"><img src="' + photoImageUrl + '" alt=""><div class="instagram-overlay"><div class="dt h100"><div class="dtc vab p-- sm-p- md-p"><h3 class="instagram-heading">@' + photoUsername + '</h3><div class="instagram-date">' + photoDateFormatted + '</div></div></div></div></a>').appendTo(instagramWrapper);

              }
            }
          }, 1000);

        }).fail(function(error) { // Epic fail :(

          // Print error to the console
          console.log(error);

        });

      }

    });

  } // end Home: Lazyload Instagram AJAX

  // Persons: Pet Mode
  personToggle.on('click', function() {
    var $this = $(this);
    
    $this.toggleClass('is-active');
    body.toggleClass('is-pet-mode');

    // Flip all at once
    person.toggleClass('is-flipped');

    // Meow! Then play background music
    meow.play();

    // Flip one at a time
    /*person.each(function(i) {
      var $this = $(this);
      setTimeout(function() {
        $this.toggleClass('is-flipped');
      }, 250 * i);
    });*/
  });

  // Forms: Animated Form Fields
  formAnimatedInput.each(function() {
    var $this = $(this);

    $this.on('focus', function() {
      $this.addClass('is-filled');
    });
    
    $this.on('blur', function() {
      if($this.val().length == 0) {
        $this.removeClass('is-filled');
      }
    });
  });

  // Services: Header Background Image Effect (jQuery Plugin)
  bgMouseoverEffect.each(function() {
    var el = $(this),
        h = el.outerHeight(),
        w = el.outerWidth(),
        strength = 25,
        scale = 1.05,
        animationSpeed = '250ms',
        contain = true,
        sh = strength / h,
        sw = strength / w,
        has_touch = 'ontouchstart' in document.documentElement;

    if (contain === true) {
      el.css({
        overflow: "hidden"
      });
    }

    // Insert new container so that background can be contained when scaled
    el.prepend('<div class="bg-mouseover-effect-container"></div>');

    // Add background to newly added container
    if(el.data('bg-mouseover-effect') !== undefined) {
      el.find('> .bg-mouseover-effect-container').css({
        background: 
          'url("' + el.data('bg-mouseover-effect') + '") no-repeat center center', 
          'background-size': 'cover'
      });
    }

    // If mobile, use the accelerometer!
    if(has_touch || screen.width <= 768) {
      window.addEventListener('devicemotion', deviceMotionHandler, false);

      function deviceMotionHandler(eventData) {
        var accX = Math.round(event.accelerationIncludingGravity.x * 10) / 10,
            accY = Math.round(event.accelerationIncludingGravity.y * 10) / 10,
            xA = -(accX / 10) * strength,
            yA = -(accY / 10) * strength,
            newX = -(xA * 2),
            newY = -(yA * 2);

        el.find('> .bg-mouseover-effect').css({
          "-webkit-transform": "matrix(" + scale + ",0,0," + scale + "," + newX + "," + newY + ")",
          "-moz-transform": "matrix(" + scale + ",0,0," + scale + "," + newX + "," + newY + ")",
          "-o-transform": "matrix(" + scale + ",0,0," + scale + "," + newX + "," + newY + ")",
          "transform": "matrix(" + scale + ",0,0," + scale + "," + newX + "," + newY + ")"
        });
      }
    }
    // Otherwise, use mouseenter
    else {
      el.on('mouseenter', function(e) {
        if(scale != 1) {
          el.addClass('bg-mouseover-effect-entering');
        }
        el.find('> .bg-mouseover-effect-container').css({
          "-webkit-transform": "matrix(" + scale + ",0,0," + scale + ",0,0)",
          "-moz-transform": "matrix(" + scale + ",0,0," + scale + ",0,0)",
          "-o-transform": "matrix(" + scale + ",0,0," + scale + ",0,0)",
          "transform": "matrix(" + scale + ",0,0," + scale + ",0,0)",
          "-webkit-transition": "-webkit-transform " + animationSpeed + " ease-in-out",
          "-moz-transition": "-moz-transform " + animationSpeed + " ease-in-out",
          "-o-transition": "-o-transform " + animationSpeed + " ease-in-out",
          "transition": "transform " + animationSpeed + " ease-in-out"
        }).on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
          // This will signal the mousemove below to execute when the scaling animation stops
          el.removeClass('bg-mouseover-effect-entering');
        });
      }).on('mousemove', function(e) {
        // Prevent transition from causing the movement of the background to lag
        if(!el.hasClass('bg-mouseover-effect-entering') && !el.hasClass('bg-mouseover-effect-exiting')) {
          var pageX = e.pageX || e.clientX,
              pageY = e.pageY || e.clientY,
              pageX = (pageX - el.offset().left) - (w / 2),
              pageY = (pageY - el.offset().top) - (h / 2),
              newX = ((sw * pageX)) * - 1,
              newY = ((sh * pageY)) * - 1;
          // Use matrix to move the background from its origin
          // Also, disable transition to prevent lag
          el.find('> .bg-mouseover-effect-container').css({
            "-webkit-transform": "matrix(" + scale + ",0,0," + scale + "," + newX + "," + newY + ")",
            "-moz-transform": "matrix(" + scale + ",0,0," + scale + "," + newX + "," + newY + ")",
            "-o-transform": "matrix(" + scale + ",0,0," + scale + "," + newX + "," + newY + ")",
            "transform": "matrix(" + scale + ",0,0," + scale + "," + newX + "," + newY + ")",
            "-webkit-transition": "none",
            "-moz-transition": "none",
            "-o-transition": "none",
            "transition": "none"
          });
        }
      }).on('mouseleave', function(e) {
        if(scale != 1) {
          el.addClass('bg-mouseover-effect-exiting');
        }
        // Same condition applies as mouseenter. Rescale the background abck to its original scale
        el.addClass('bg-mouseover-effect-exiting').find('> .bg-mouseover-effect-container').css({
          "-webkit-transform": "matrix(1,0,0,1,0,0)",
          "-moz-transform": "matrix(1,0,0,1,0,0)",
          "-o-transform": "matrix(1,0,0,1,0,0)",
          "transform": "matrix(1,0,0,1,0,0)",
          "-webkit-transition": "-webkit-transform " + animationSpeed + " ease-in-out",
          "-moz-transition": "-moz-transform " + animationSpeed + " ease-in-out",
          "-o-transition": "-o-transform " + animationSpeed + " ease-in-out",
          "transition": "transform " + animationSpeed + " ease-in-out"
        }).on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
          el.removeClass('bg-mouseover-effect-exiting');
        });
      });
    }
  });
});
