var gulp        = require('gulp');
var browsersync = require('browser-sync');
var config      = require('../../config').images;

/**
 * Resize images
 */
gulp.task('images', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
