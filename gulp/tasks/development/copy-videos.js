var gulp   = require('gulp');
var config = require('../../config').copyvideos.development;

/**
 * Copy videos to folder
 */
gulp.task('copy:videos', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
