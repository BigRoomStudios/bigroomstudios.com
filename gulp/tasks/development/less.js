var gulp         = require('gulp');
var browsersync  = require('browser-sync');
var less         = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var handleErrors = require('../../util/handleErrors');
var config       = require('../../config');

/**
 * Generate CSS from LESS
 */
gulp.task('less', function() {
  browsersync.notify('Compiling LESS');

  return gulp.src(config.less.main)
    .pipe(less())
    .on('error', handleErrors)
    .pipe(autoprefixer(config.autoprefixer))
    .pipe(gulp.dest(config.less.dest))
    .pipe(gulp.dest(config.kss.dest + '/assets'));
});
