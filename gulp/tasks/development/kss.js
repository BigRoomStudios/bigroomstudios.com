var gulp         = require('gulp');
var browsersync  = require('browser-sync');
var kss          = require('gulp-kss');
var handleErrors = require('../../util/handleErrors');
var config       = require('../../config').kss;

/**
 * Generate KSS Styleguide from LESS
 */
gulp.task('kss', ['less'], function() {
  gulp.src(config.src)
    .pipe(kss(config.options))
    .on('error', handleErrors)
    .pipe(gulp.dest(config.dest));
});
