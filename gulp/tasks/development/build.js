var gulp        = require('gulp');
var runSequence = require('run-sequence');

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('build', function(callback) {
  runSequence(
    'delete',
    [
      'kss',
      'scripts',
      'images',
      'copy:images',
      'copy:fonts',
      'copy:audios',
      'copy:videos'
    ],
    'jekyll',
    callback
  );
});
