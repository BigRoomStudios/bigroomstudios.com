var gulp   = require('gulp');
var config = require('../../config').copyaudios.development;

/**
 * Copy audios to folder
 */
gulp.task('copy:audios', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
