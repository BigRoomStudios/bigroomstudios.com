var gulp   = require('gulp');
var config = require('../../config').watch;

/**
 * Start browsersync task and then watch files for changes
 */
gulp.task('watch', ['browsersync'], function() {
  gulp.watch(config.jekyll,  ['jekyll-rebuild']);
  gulp.watch(config.less,    ['kss']);
  gulp.watch(config.kss,     ['kss']);
  gulp.watch(config.scripts, ['scripts', 'jshint']);
  gulp.watch(config.images,  ['jekyll-rebuild', 'copy:images']);
  gulp.watch(config.fonts,   ['jekyll-rebuild', 'copy:fonts']);
  gulp.watch(config.audios,  ['jekyll-rebuild', 'copy:audios']);
  gulp.watch(config.videos,  ['jekyll-rebuild', 'copy:videos']);
});
