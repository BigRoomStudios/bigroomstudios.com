var gulp             = require('gulp');
var awspublish       = require('gulp-awspublish');
var awspublishRouter = require('gulp-awspublish-router');
var cloudfront       = require('gulp-cloudfront');
var fs               = require('fs');
var parallelize      = require('concurrent-transform');
var config           = require('../../config').s3;

gulp.task('s3', function() {
  // Grab credentials from hidden file
  var credentials = JSON.parse(fs.readFileSync(config.credfile, 'utf8'));
  // Create a new publisher
  var publisher = awspublish.create(credentials);
  // Set the proper headers
  var headers = {
    'Cache-Control': 'max-age=0, no-transform, public',
    'Content-Encoding': 'gzip'
  };

  return gulp.src(config.src)
    .pipe(awspublishRouter({
      routes: {
        '^assets/(?:.+)\\.(?:js|css)$': {
          key: '$&',
          headers: {
            'Cache-Control': 'max-age=315360000, no-transform, public',
            'Content-Encoding': 'gzip'
          }
        },

        '^assets/(?:.+)\\.(?:jpg|png|gif|svg)$': {
          key: '$&',
          headers: {
            'Cache-Control': 'max-age=315360000, no-transform, public',
            'Content-Encoding': 'gzip'
          }
        },

        '^assets/fonts/(?:.+)\\.(?:eot|svg|ttf|woff|woff2)$': {
          key: '$&',
          headers: {
            'Cache-Control': 'max-age=315360000, no-transform, public'
          }
        },

        '^.+\\.html': {
          key: '$&',
          headers: {
            'Cache-Control': 'max-age=0, no-transform, public',
            'Content-Encoding': 'gzip'
          }
        },
        '^.+$': '$&'
      }
    }))
    // Gzip the files for MOAR SPEEDZ
    .pipe(awspublish.gzip())
    // Upload files in parallel
    .pipe(parallelize(publisher.publish(), 30))
    // Cache files in system cache so you don't have to recheck all files
    .pipe(publisher.cache())

    // Sync contents of S3 bucket and local environment
    // This deletes everything in S3 bucket that is not on local
    // NOTE: This breaks on delete, which is why its currently commented out
    // .pipe(publisher.sync())

    // Print the output
    .pipe(awspublish.reporter({
      states: [
        'create',
        'update',
        'delete'
      ]
    }))
    // Update the default root object
    .pipe(cloudfront(credentials));
});
