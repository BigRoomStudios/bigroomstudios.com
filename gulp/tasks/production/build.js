var gulp        = require('gulp');
var runSequence = require('run-sequence');

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('build:production', function(callback) {
  runSequence(
    'delete',
    'jekyll:production',
    [
      'less',
      'scripts',
      'copy:images',
      'copy:fonts',
      'copy:audios',
      'copy:videos'
    ],
    [
      'kss',
      'copy:kss'
    ],
    'optimize:css',
    'optimize:js',
    'optimize:images',
    'optimize:html',
    'copy:fonts:production',
    'copy:audios:production',
    'copy:videos:production',
    'revision',
    'rev:collect',
    // [
    //   'gzip'
    // ],
    callback
  );
});
