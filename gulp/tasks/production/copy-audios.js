var gulp   = require('gulp');
var config = require('../../config').copyaudios.production;

/**
 * Copy audios to folder
 */
gulp.task('copy:audios:production', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
