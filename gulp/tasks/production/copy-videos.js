var gulp   = require('gulp');
var config = require('../../config').copyvideos.production;

/**
 * Copy videos to folder
 */
gulp.task('copy:videos:production', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
