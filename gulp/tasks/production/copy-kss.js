var gulp   = require('gulp');
var config = require('../../config').kss;

/**
 * Copy KSS Styleguide to production folder
 */
gulp.task('copy:kss', function() {
  return gulp.src(config.copy.src)
    .pipe(gulp.dest(config.copy.dest));
});
