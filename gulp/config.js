var src               = 'app';
var srcAssets         = 'app/_assets';
var build             = 'build';
var development       = 'build/development';
var developmentAssets = 'build/assets';
var production        = 'build/production';
var productionAssets  = 'build/production/assets';

module.exports = {
  browsersync: {
    development: {
      server: {
        baseDir: [development, build, src]
      },
      port: 9999,
      xip: true,
      files: [
        developmentAssets + '/css/*.css',
        developmentAssets + '/js/*.js',
        developmentAssets + '/images/**',
        development + '/styleguide/**'
      ],
      notify: {
        styles: [ 'display: hidden; padding: 12px; font-family: sans-serif; position: fixed; font-size: 14px; z-index: 10000; left: 50%; bottom: 0; width: 200px; margin: 0; margin-left: -100px; border-top-left-radius: 3px; border-top-right-radius: 3px; color: #fff; text-align: center; background-color: rgb(27, 32, 50);' ]
      }
    },
    production: {
      server: {
        baseDir: [production]
      },
      port: 9998,
      xip: true
    }
  },
  delete: {
    src: [developmentAssets]
  },
  jekyll: {
    development: {
      src: src,
      dest: development,
      config: '_config.yml'
    },
    production: {
      src: src,
      dest: production,
      config: '_config.yml,_config.build.yml'
    }
  },
  less: {
    src: srcAssets + '/less/**/*.less',
    main: srcAssets + '/less/main.less',
    dest: developmentAssets + '/css',
    options: {
      filename: srcAssets + '/less/main.less'
    }
  },
  kss: {
    src: srcAssets + '/less/**/*.less',
    dest: development + '/styleguide',
    options: {
      overview: src + '/_styleguide/overview.md',
      templateDirectory: src + '/_styleguide'
    },
    copy: {
      src: development + '/styleguide/**/*',
      dest: production + '/styleguide'
    }
  },
  autoprefixer: {
    browsers: [
      'last 2 versions',
      'safari 5',
      'ie 8',
      'ie 9',
      'opera 12.1',
      'ios 6',
      'android 4'
    ],
    cascade: true
  },
  browserify: {
    // Enable source maps
    debug: true,
    // Additional file extensions to make optional
    extensions: ['.coffee', '.hbs'],
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: './' + srcAssets + '/javascripts/application.js',
      dest: developmentAssets + '/js',
      outputName: 'application.js'
    }, {
      entries: './' + srcAssets + '/javascripts/head.js',
      dest: developmentAssets + '/js',
      outputName: 'head.js'
    }]
  },
  images: {
    src: srcAssets + '/images/**/*',
    dest: developmentAssets + '/images'
  },
  gzip: {
    src: [
      production + '/**/*.{html,xml,json,css,js}'
    ],
    dest: production,
    options: {}
  },
  copyfonts: {
    development: {
      src:  srcAssets + '/fonts/*',
      dest: developmentAssets + '/fonts'
    },
    production: {
      src:  developmentAssets + '/fonts/*',
      dest: productionAssets + '/fonts'
    }
  },
  copyaudios: {
    development: {
      src:  srcAssets + '/audios/*',
      dest: developmentAssets + '/audios'
    },
    production: {
      src:  developmentAssets + '/audios/*',
      dest: productionAssets + '/audios'
    }
  },
  copyvideos: {
    development: {
      src:  srcAssets + '/videos/*',
      dest: developmentAssets + '/videos'
    },
    production: {
      src:  developmentAssets + '/videos/*',
      dest: productionAssets + '/videos'
    }
  },
  watch: {
    jekyll: [
      '_config.yml',
      '_config.build.yml',
      'stopwords.txt',
      src + '/_data/**/*.{json,yml,csv}',
      src + '/_includes/**/*.{html,xml}',
      src + '/_layouts/*.html',
      src + '/_locales/*.yml',
      src + '/_plugins/*.rb',
      src + '/_posts/*.{markdown,md}',
      src + '/**/*.{html,markdown,md,yml,json,txt,xml}',
      src + '/*'
    ],
    less: srcAssets + '/less/**/*.less',
    scripts: srcAssets + '/javascripts/**/*.js',
    images: srcAssets + '/images/**/*',
    videos: srcAssets + '/videos/**/*',
    kss: src + '/_styleguide/**/*'
  },
  jshint: {
    src: srcAssets + '/javascripts/*.js'
  },
  optimize: {
    css: {
      src: developmentAssets + '/css/*.css',
      dest: productionAssets + '/css/',
      options: {
        keepSpecialComments: 0
      }
    },
    js: {
      src: developmentAssets + '/js/*.js',
      dest: productionAssets + '/js/',
      options: {}
    },
    images: {
      src: developmentAssets + '/images/**/*.{jpg,jpeg,png,gif,svg}',
      dest: productionAssets + '/images/',
      options: {
        optimizationLevel: 5,
        progessive: true,
        interlaced: true
      }
    },
    html: {
      src: production + '/**/*.html',
      dest: production,
      options: {
        collapseWhitespace: true,
        conservativeCollapse: true
      }
    }
  },
  revision: {
    src: {
      assets: [
        productionAssets + '/css/*.css',
        productionAssets + '/js/*.js',
        productionAssets + '/images/**/*',
        productionAssets + '/videos/**/*'
      ],
      base: production
    },
    dest: {
      assets: production,
      manifest: {
        name: 'manifest.json',
        path: productionAssets
      }
    }
  },
  collect: {
    src: [
      productionAssets + '/manifest.json',
      production + '/**/*.{html,xml,txt,json,css,js}',
      '!' + production + '/feed.xml'
    ],
    dest: production
  },
  rsync: {
    src: production + '/**',
    options: {
      destination: '~/path/to/my/website/root/',
      root: production,
      hostname: 'mydomain.com',
      username: 'user',
      incremental: true,
      progress: true,
      relative: true,
      emptyDirectories: true,
      recursive: true,
      clean: true,
      exclude: ['.DS_Store'],
      include: []
    }
  },
  s3: {
    src: production + '/**',
    credfile: './aws-credentials.json'
  }
};
