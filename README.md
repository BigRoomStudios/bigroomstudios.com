# Big Room Studios

[http://bigroomstudios.com](http://bigroomstudios.com)

This is the official repository for the Big Room Studios website. **As of 3.0.0**, this project is built with [Jekyll](http://jekyllrb.com/), using [gulp-tutorial](https://github.com/kogakure/gulp-tutorial/) as a starting point.

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Resources](#resources)
- [Developing](#developing)
  - [Creating a Branch](#creating-a-branch)
  - [Making Changes](#making-changes)
  - [Testing Production](#testing-production)
  - [Submitting a Pull Request](#submitting-a-pull-request)
- [Reviewing](#reviewing)
- [Deploying](#deploying)
- [Known Issues](#known-issues)

# Getting Started

The following instructions will help you get up and running, so that you can start working on this project from your local machine.

## Prerequisites

This project depends on some software that must already be installed on your machine before proceeding.

- [Git](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git) (Mac users should already have this)
- [Ruby](https://www.ruby-lang.org/en/documentation/installation/) (Mac users should already have this)
- [Bundler](http://bundler.io/)

  After installing Git and Ruby, you can install Bundler by running the following command(s)*:

  ```
  gem install bundler
  ```

  *Mac users may need to use `sudo gem install bundler` instead.

- [NodeJS](http://nodejs.org/) (Use [the installer](http://nodejs.org/download/))
- [Bower](http://bower.io/)

  After installing Git, Ruby, and Node, you can install Bower by running the following command(s)*:

  ```
  npm install -g bower
  ```

  *Mac users may need to use `sudo npm install -g bower` instead.

## Installation

Once you have the above tools installed, you can [clone this repository](http://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository#Cloning-an-Existing-Repository) to your local machine, where you will be making your changes.

1. Create a directory for this project on your machine, navigate to it, and clone this repository from GitHub into your directory by running the following command(s):

  ```
  cd path/to/your/projects/folder
  mkdir bigroomstudios.com; cd bigroomstudios.com
  git clone https://github.com/BigRoomStudios/bigroomstudios.com.git .
  ```

2. Install project dependencies by running the following command(s)*:

  ```
  bundle
  npm install
  bower install
  ```

*If the `npm install` returns errors, then just run it again and “everything should be okay”.

3. Build the site and spin up a local server by running the following command(s) from your terminal:

  ```
  gulp
  ```

4. Navigate to [http://127.0.0.1.xip.io:9999](http://127.0.0.1.xip.io:9999) in your browser and begin working!*

  *This project uses [BrowserSync](http://www.browsersync.io/) and [xip.io](http://xip.io/) to automatically refresh your web browser on save, and to synchronize file changes and basic browser interactions (scrolling, clicking) across multiple devices. After running `gulp` in the terminal, you will see an "External URL" (e.g. [http://192.168.1.114.xip.io:9999](http://192.168.1.114.xip.io:9999)) that you can use for your various devices. Simply navigate to this URL in the browser of another device connected to your network, and watch your changes and interactions happen across multiple devices at the same time!

## Resources

If you are running Windows on your machine, and you’ve run into problems with the above, then you might find these guides useful:

- [Jekyll on Windows](http://jekyllrb.com/docs/windows/)
- [Run Jekyll on Windows](http://jekyll-windows.juthilo.com/)

# Developing

There are some things to be aware of before you begin working on this project. In an effort to keep things somewhat organized, a basic workflow is outlined below.

## Creating a Branch

**As of 3.0.0**, we will be using the "shared repository model" to make changes to this project. This means that you are expected to [create a branch](https://help.github.com/articles/creating-and-deleting-branches-within-your-repository/) and make all of your commits to that branch. When you are finished working, you [submit a pull request]() and contact another developer who will review, approve, and merge your work into the master branch.

Once you’ve got the project running successfully on your local machine, you should create a branch from master that will contain all of the work you plan on submitting for review. Running the following command(s) will create and checkout a new branch so that you can begin working on it:

```
git checkout -b my_branch_name
```

## Making Changes

After you’ve created and checked out the new branch for your upcoming work, you will want to run the following command(s) to build the development environment:

```
gulp
```

Navigate to [http://127.0.0.1.xip.io:9999](http://127.0.0.1.xip.io:9999) in your browser, and begin working!

While working, try to keep your commits short, comprehensible, and logical. Make sure you are only committing changes to the branch that you have just created.

```
git commit -m "updated readme"
git commit -m "added hero object; updated styleguide"
```

## Testing Production

You can build a production-ready version of this project in your local environment to see how your changes will be reflected in the actual production build. Run the following command(s) to delete any existing builds, build a production-ready version with your latest changes, and spin up a local server:

```
gulp publish
```

Navigate to [http://127.0.0.1.xip.io:9998](http://127.0.0.1.xip.io:9998) in your browser to view your local production build.

## Submitting a Pull Request

If everything looks good in your local production build, and you are ready to submit your changes for review, you will want to push your branch to the remote repository, like so:

```
git push origin my_branch_name
```

At this point, you will want to contact another developer to review your code, and approve it. Once approved, the developer will follow the directions below to build and deploy the production site.

# Reviewing

If you have been asked to review and approve someone else’s code, this section is for you. This assumes you already have your local environment setup. If not, please follow the steps above in the "[Getting Started](#getting-started)" section.

First, you will want to get the latest changes/branches from the remote repository, and switch to the branch in question, by running the following command(s) from your project clone’s root directory:

```
cd path/to/your/projects/folder
git pull
git checkout branch_name_that_needs_reviewing
```

Next, you will want to build a production version of the project to review by running the following command(s):

```
gulp publish
```

Test the new changes, and if everything looks good, merge this branch into master and push it to the remote repository by running the following command(s):

```
git checkout master
git merge branch_name_that_needs_reviewing
git push origin master
```

# Deploying

The live website is hosted on Amazon S3, and requires that you create a file on your local machine that stores the credentials for the S3 bucket(s).

First, create a file called `aws-credentials.json` in the root directory of this project on your local machine.

Add the following line(s) to `aws-credentials.json`:

```
{
  "key": "YOUR_KEY",
  "secret": "YOUR_SECRET",
  "bucket": "YOUR_DOMAIN",
  "region": "YOUR_REGION"
}
```

Make sure to swap the placeholders in the above example with the actual credentials for this project’s S3 bucket. You can get this information from your supervisor. This file is not committed to the repository on purpose, for security reasons.

After you’ve saved your `aws-credentials.json` file, you should be all set to deploy changes. We currently have two buckets: `staging.bigroomstudios.com`, and `bigroomstudios.com`. You typically will want to deploy to staging first to check changes, and if it looks good, change your `aws-credentials.json` file and re-deploy to the live site. To deploy this project to Amazon S3, run the following command(s):

```
gulp deploy
```

# Known Issues

Issues are logged and tracked in using GitHub’s [Issues](https://github.com/BigRoomStudios/bigroomstudios.com/issues) feature in this repo.
